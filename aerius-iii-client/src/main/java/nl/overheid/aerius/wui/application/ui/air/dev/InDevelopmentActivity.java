package nl.overheid.aerius.wui.application.ui.air.dev;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.wui.application.ui.air.AirQualitySubActivity;
import nl.overheid.aerius.wui.application.ui.air.AirQualityView;
import nl.overheid.aerius.wui.application.ui.deposition.DepositionSubActivity;

public class InDevelopmentActivity implements AirQualitySubActivity, DepositionSubActivity {
  @Inject
  public InDevelopmentActivity(@Assisted final AirQualityView view) {
    view.setComponent(InDevelopmentViewFactory.get(), this);
  }
}
