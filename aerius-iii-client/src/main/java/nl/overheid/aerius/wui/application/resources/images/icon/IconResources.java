package nl.overheid.aerius.wui.application.resources.images.icon;

import com.google.gwt.resources.client.ClientBundle.Source;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.DataResource.MimeType;

public interface IconResources {
  @Source("main-menu-left-emission-sources.svg")
  @MimeType("image/svg+xml")
  DataResource iconSources();

  @Source("main-menu-left-calculation-points.svg")
  @MimeType("image/svg+xml")
  DataResource iconReceptors();

  @Source("main-menu-left-notice-permits.svg")
  @MimeType("image/svg+xml")
  DataResource iconMeasures();

  @Source("main-menu-left-settings.svg")
  @MimeType("image/svg+xml")
  DataResource iconPreferences();

  @Source("main-menu-left-results.svg")
  @MimeType("image/svg+xml")
  DataResource iconResults();

  @Source("main-menu-left-map-layers.svg")
  @MimeType("image/svg+xml")
  DataResource iconLayers();
}
