package nl.overheid.aerius.wui.application.ui.air.receptors.create;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;

import nl.overheid.aerius.wui.application.components.dummy.SimpleSecondaryDummyComponent;
import nl.overheid.aerius.wui.application.components.views.DefaultTitleView;
import nl.overheid.aerius.wui.vue.BasicVueView;

@Component(components = {
    DefaultTitleView.class,
    SimpleSecondaryDummyComponent.class })
public class ReceptorCreateView extends BasicVueView {
  @Prop ReceptorCreateActivity presenter;
}
