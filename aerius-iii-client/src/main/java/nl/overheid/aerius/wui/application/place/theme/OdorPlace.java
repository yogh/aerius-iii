package nl.overheid.aerius.wui.application.place.theme;

import nl.overheid.aerius.wui.application.place.MainThemePlace;

public class OdorPlace extends MainThemePlace {
  public OdorPlace() {
    super(ThemeTokenizers.ODOR);
  }
}
