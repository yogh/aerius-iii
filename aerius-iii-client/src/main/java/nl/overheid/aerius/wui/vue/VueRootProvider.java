package nl.overheid.aerius.wui.vue;

import com.google.inject.Inject;

public class VueRootProvider {
  public static class VueRoot extends StaticVueProvider<VueRootView> {
    @Inject
    public VueRoot(final VueRootViewFactory factory) {
      super(factory);
    }
  }
}
