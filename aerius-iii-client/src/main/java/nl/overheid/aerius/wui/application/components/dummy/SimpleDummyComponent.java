package nl.overheid.aerius.wui.application.components.dummy;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

@Component(name = "dummy")
public class SimpleDummyComponent implements IsVueComponent {
  @Data String message = "dummy-bound";

  @Data Integer count = 0;
}
