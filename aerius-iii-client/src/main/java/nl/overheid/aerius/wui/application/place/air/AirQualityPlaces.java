package nl.overheid.aerius.wui.application.place.air;

import java.util.function.Supplier;

import nl.overheid.aerius.wui.application.place.MainThemePlace;
import nl.overheid.aerius.wui.application.util.ThemeOption;
import nl.overheid.aerius.wui.place.PlaceTokenizer;

public final class AirQualityPlaces {
  private AirQualityPlaces() {}

  public static class AirQualityPlace extends MainThemePlace {
    public static class Tokenizer<T extends AirQualityPlace> extends MainThemePlace.Tokenizer<T> {
      public Tokenizer(final Supplier<T> provider, final String... postfix) {
        super(provider, ThemeOption.AIR_QUALITY, postfix);
      }
    }

    public <P extends AirQualityPlace> AirQualityPlace(final PlaceTokenizer<P> tokenizer) {
      super(tokenizer);
    }
  }

  public static class SourcePlace extends AirQualityPlace {
    public SourcePlace() {
      this(AirQualityTokenizers.SOURCE);
    }

    public <P extends SourcePlace> SourcePlace(final PlaceTokenizer<P> tokenizer) {
      super(tokenizer);
    }
  }

  public static class SourceCreatePlace extends SourcePlace {
    public SourceCreatePlace() {
      super(AirQualityTokenizers.SOURCE_CREATE);
    }
  }

  public static class SourceImportPlace extends SourcePlace {
    public SourceImportPlace() {
      super(AirQualityTokenizers.SOURCE_IMPORT);
    }
  }

  public static class SourceListPlace extends SourcePlace {
    public SourceListPlace() {
      super(AirQualityTokenizers.SOURCE_LIST);
    }
  }

  public static class CalculationPointPlace extends AirQualityPlace {
    public CalculationPointPlace() {
      this(AirQualityTokenizers.CALCULATION_POINT);
    }

    public <P extends CalculationPointPlace> CalculationPointPlace(final PlaceTokenizer<P> tokenizer) {
      super(tokenizer);
    }
  }

  public static class CalculationPointCreatePlace extends CalculationPointPlace {
    public CalculationPointCreatePlace() {
      super(AirQualityTokenizers.RECEPTOR_CREATE);
    }
  }

  public static class CalculationPointListPlace extends CalculationPointPlace {
    public CalculationPointListPlace() {
      super(AirQualityTokenizers.RECEPTOR_LIST);
    }
  }

  public static class MeasurePlace extends AirQualityPlace {
    public MeasurePlace() {
      this(AirQualityTokenizers.MEASURE);
    }

    public <P extends MeasurePlace> MeasurePlace(final PlaceTokenizer<P> tokenizer) {
      super(tokenizer);
    }
  }

  public static class PreferencePlace extends AirQualityPlace {
    public PreferencePlace() {
      this(AirQualityTokenizers.PREFERENCE);
    }

    public <P extends PreferencePlace> PreferencePlace(final PlaceTokenizer<P> tokenizer) {
      super(tokenizer);
    }
  }

  public static class ResultPlace extends AirQualityPlace {
    public ResultPlace() {
      this(AirQualityTokenizers.RESULT);
    }

    public <P extends ResultPlace> ResultPlace(final PlaceTokenizer<P> tokenizer) {
      super(tokenizer);
    }
  }

  public static class LayerPlace extends AirQualityPlace {
    public LayerPlace() {
      this(AirQualityTokenizers.LAYER);
    }

    public <P extends ResultPlace> LayerPlace(final PlaceTokenizer<P> tokenizer) {
      super(tokenizer);
    }
  }
}
