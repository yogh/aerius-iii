package nl.overheid.aerius.wui.application.resources.images.misc;

import com.google.gwt.resources.client.ClientBundle.Source;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.DataResource.MimeType;

public interface MiscellaneousResources {
  @Source("ch-close-col.svg")
  @MimeType("image/svg+xml")
  DataResource collapseButton();
}
