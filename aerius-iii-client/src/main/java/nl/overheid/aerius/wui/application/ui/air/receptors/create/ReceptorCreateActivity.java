package nl.overheid.aerius.wui.application.ui.air.receptors.create;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.wui.application.components.dummy.SimpleDummyComponentFactory;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointListPlace;
import nl.overheid.aerius.wui.application.ui.air.AirQualitySubActivity;
import nl.overheid.aerius.wui.application.ui.air.AirQualityView;
import nl.overheid.aerius.wui.place.PlaceController;

public class ReceptorCreateActivity implements AirQualitySubActivity {
  @Inject PlaceController placeController;

  @Inject
  public ReceptorCreateActivity(@Assisted final AirQualityView view) {
    view.setComponent(ReceptorCreateViewFactory.get(), this);
    view.setMap(SimpleDummyComponentFactory.get());
  }

  public void goToList() {
    placeController.goTo(new CalculationPointListPlace());
  }
}
