package nl.overheid.aerius.wui.application.util;

import com.google.auto.value.AutoValue;
import com.google.gwt.resources.client.DataResource;

@AutoValue
public abstract class Theme {
  public abstract ThemeOption getOption();

  public abstract DataResource getIcon();

  public abstract boolean isEnabled();

  static Theme create(final ThemeOption option, final DataResource icon, final boolean enabled) {
    return new AutoValue_Theme(option, icon, enabled);
  }
}
