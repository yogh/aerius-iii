package nl.overheid.aerius.wui.application.components.nav;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import nl.overheid.aerius.wui.application.components.logo.AeriusLogoComponent;

@Component(name = "aer-header-bar", components = { AeriusLogoComponent.class })
public class HeaderComponent implements IsVueComponent {}
