package nl.overheid.aerius.wui.application.ui.unsupported;

import com.axellience.vuegwt.core.annotations.component.Component;

import nl.overheid.aerius.wui.vue.BasicVueView;

@Component
public class NotSupportedView extends BasicVueView {}
