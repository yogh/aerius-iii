package nl.overheid.aerius.wui.vue;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import nl.overheid.aerius.wui.application.i18n.ApplicationMessages;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.resources.ImageResources;
import nl.overheid.aerius.wui.application.resources.R;

@Component(hasTemplate = false)
public abstract class BasicVueComponent implements IsVueComponent {
  @Data public ApplicationMessages i18n = M.messages();

  @Data public ImageResources img = R.images();

  public void destroy() {
    vue().$destroy();
    vue().$el().parentNode.removeChild(vue().$el());
  }
}
