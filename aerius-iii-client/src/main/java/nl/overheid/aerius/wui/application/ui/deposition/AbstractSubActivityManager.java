package nl.overheid.aerius.wui.application.ui.deposition;

import java.util.function.Consumer;

import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.place.Place;
import nl.overheid.aerius.wui.widget.HasEventBus;

public abstract class AbstractSubActivityManager<V, S> {
  private EventBus eventBus;

  private Consumer<Place> redirector;

  private V view;
  private Place place;

  private boolean delegate;

  public boolean delegate(final EventBus eventBus, final Place place, final Consumer<Place> redirector) {
    this.eventBus = eventBus;
    this.place = place;
    this.redirector = redirector;

    return tryCanDelegate();
  }

  public void setView(final V view) {
    this.view = view;

    doDelegate();
  }

  private void doDelegate() {
    if (!delegate) {
      return;
    }

    delegate = false;
    final S act = getActivity(place, view);
    if (act == null) {
      return;
    }

    if (act instanceof HasEventBus) {
      ((HasEventBus) act).setEventBus(eventBus);
    }
  }

  private S redirect(final Place place) {
    redirector.accept(place);
    return null;
  }

  private boolean tryCanDelegate() {
    final Place redirect = getRedirect(place);

    if (redirect == null) {
      scheduleDelegate();
    } else {
      redirect(redirect);
    }

    return redirect == null;
  }

  private void scheduleDelegate() {
    delegate = true;
    if (view != null) {
      doDelegate();
    }
  }

  protected abstract Place getRedirect(Place place);

  protected abstract S getActivity(Place place, V view);
}
