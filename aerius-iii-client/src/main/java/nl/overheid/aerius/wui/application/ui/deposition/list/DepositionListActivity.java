package nl.overheid.aerius.wui.application.ui.deposition.list;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.wui.application.components.dummy.SimpleSecondaryDummyComponentFactory;
import nl.overheid.aerius.wui.application.place.deposition.DepositionSourceImportPlace;
import nl.overheid.aerius.wui.application.place.deposition.DepositionSourceListPlace;
import nl.overheid.aerius.wui.application.ui.deposition.DepositionSubActivity;
import nl.overheid.aerius.wui.application.ui.deposition.DepositionView;
import nl.overheid.aerius.wui.place.PlaceController;

public class DepositionListActivity implements DepositionSubActivity {
  @Inject PlaceController placeController;

  @Inject
  public DepositionListActivity(@Assisted final DepositionView view, @Assisted final DepositionSourceListPlace place) {
    view.setComponent(DepositionListViewFactory.get(), this);

    view.setMap(SimpleSecondaryDummyComponentFactory.get());
  }

  public void goToImport() {
    placeController.goTo(new DepositionSourceImportPlace());
  }
}
