package nl.overheid.aerius.wui.application.ui.air;

import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasActivated;
import com.axellience.vuegwt.core.client.vue.VueComponentFactory;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;
import nl.overheid.aerius.wui.application.components.dummy.SimpleDummyComponent;
import nl.overheid.aerius.wui.application.components.dummy.SimpleSecondaryDummyComponent;
import nl.overheid.aerius.wui.application.components.nav.NavigationConfiguration;
import nl.overheid.aerius.wui.application.components.nav.NavigationItem;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceImportPlace;
import nl.overheid.aerius.wui.application.ui.air.dev.InDevelopmentView;
import nl.overheid.aerius.wui.application.ui.air.receptors.create.ReceptorCreateView;
import nl.overheid.aerius.wui.application.ui.air.receptors.list.ReceptorListView;
import nl.overheid.aerius.wui.application.ui.air.sources.create.SourceCreateView;
import nl.overheid.aerius.wui.application.ui.air.sources.list.SourceListView;
import nl.overheid.aerius.wui.application.ui.air.sources.start.SourceStartView;
import nl.overheid.aerius.wui.application.ui.main.MainView;
import nl.overheid.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.vue.AcceptsOneComponent;

@Component(components = {
    MainView.class,
    ReceptorCreateView.class,
    ReceptorListView.class,
    SourceStartView.class,
    SourceListView.class,
    SourceCreateView.class,
    InDevelopmentView.class,
    SimpleDummyComponent.class,
    SimpleSecondaryDummyComponent.class
})
public class AirQualityView implements IsVueComponent, HasActivated, AcceptsOneComponent {
  @Inject PlaceController placeController;

  @Data @JsProperty List<NavigationItem> navigation;

  @Prop EventBus eventBus;
  @Prop AirQualityPresenter presenter;

  @Data Object subPresenter;

  @Data String input;
  @Data String main;

  @JsMethod
  public void testGoToStart() {
    placeController.goTo(new SourceImportPlace());
  }

  @JsMethod
  public void testGoToCreate() {
    placeController.goTo(new SourceCreatePlace());
  }

  @Override
  public void activated() {
    presenter.setView(this);
  }

  @Override
  public <P> void setComponent(final VueComponentFactory<?> fact, final P presenter) {
    input = fact.getComponentTagName();
    subPresenter = presenter;
  }

  public void setMap(final VueComponentFactory<?> fact) {
    main = fact.getComponentTagName();
  }

  public void setNavigation(final NavigationConfiguration nav) {
    this.navigation = nav;
  }
}
