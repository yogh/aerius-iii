/*
 * Copyright Dutch Ministry of Agriculture, Nature and Food Quality
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.bootstrap;

import java.util.Date;

import com.axellience.vuegwt.core.client.VueGWT;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.core.client.Scheduler;

import nl.overheid.aerius.wui.Application;
import nl.overheid.aerius.wui.dev.GWTProd;

public class Bootstrap implements EntryPoint {
  @Override
  public void onModuleLoad() {
    GWTProd.log("Hello! Welcome to the AERIUS Console!");
    GWTProd.log(new Date().getTime() + " <Bootstrap start time.");

    VueGWT.initWithoutVueLib();

    VueGWT.onReady(() -> {
      final BootstrapLoadingView comp = BootstrapLoadingViewFactory.get().create();
      comp.vue().$mount("#bootstrap");
      GWT.runAsync(new RunAsyncCallback() {
        @Override
        public void onFailure(final Throwable caught) {
          GWTProd.log("Bootstrap failed. " + caught.getMessage());
        }

        @Override
        public void onSuccess() {
          GWTProd.log(new Date().getTime() + " <Application load start time.");

          Scheduler.get().scheduleFinally(() -> {
            comp.destroy();
            Application.A.create();
            GWTProd.log(new Date().getTime() + " <Application load complete time.");
          });
        }
      });
    });
  }
}
