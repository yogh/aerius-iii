package nl.overheid.aerius.wui.application.ui.deposition.create;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.wui.application.components.dummy.SimpleDummyComponentFactory;
import nl.overheid.aerius.wui.application.place.deposition.DepositionSourceCreatePlace;
import nl.overheid.aerius.wui.application.place.deposition.DepositionSourceListPlace;
import nl.overheid.aerius.wui.application.ui.deposition.DepositionSubActivity;
import nl.overheid.aerius.wui.application.ui.deposition.DepositionView;
import nl.overheid.aerius.wui.place.PlaceController;

public class DepositionCreateActivity implements DepositionSubActivity {
  @Inject PlaceController placeController;

  @Inject
  public DepositionCreateActivity(@Assisted final DepositionView view, @Assisted final DepositionSourceCreatePlace place) {
    view.setComponent(DepositionCreateViewFactory.get(), this);
    view.setMap(SimpleDummyComponentFactory.get());
  }

  public void goToList() {
    placeController.goTo(new DepositionSourceListPlace());
  }
}
