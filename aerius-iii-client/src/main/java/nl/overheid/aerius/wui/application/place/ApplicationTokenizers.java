package nl.overheid.aerius.wui.application.place;

import nl.overheid.aerius.wui.place.ApplicationPlace;
import nl.overheid.aerius.wui.place.PlaceTokenizer;

public class ApplicationTokenizers {
  private static final String OVERVIEW_KEY = "overview";

  public static final PlaceTokenizer<OverviewPlace> OVERVIEW = new ApplicationPlace.Tokenizer<>(
      () -> new OverviewPlace(), OVERVIEW_KEY);
}
