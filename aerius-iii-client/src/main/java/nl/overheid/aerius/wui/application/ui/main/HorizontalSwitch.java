package nl.overheid.aerius.wui.application.ui.main;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;

import elemental2.dom.Element;
import elemental2.dom.HTMLElement;
import jsinterop.annotations.JsMethod;

@Component
public class HorizontalSwitch implements IsVueComponent {
  private int previousWidth;
  private int newWidth;

  @JsMethod
  public void beforeLeave(final Element el) {
    previousWidth = el.clientWidth;
    GWT.log("Storing previous width: " + previousWidth);
  }

  @JsMethod
  public void beforeEnter(final Element el) {
    newWidth = el.clientWidth;
    GWT.log("Storing new width: " + newWidth);
    setWidth(el, previousWidth);
  }

  @JsMethod
  public void enter(final Element el) {
    setWidth(el, previousWidth);
    Scheduler.get().scheduleDeferred(() -> {
      GWT.log("Enteringgggggg to..");
      clean(el);
      // setWidth(el, ((HTMLElement) el).clientWidth);
    });
  }

  @JsMethod
  public void afterEnter(final Element el) {
    clean(el);
  }

  @JsMethod
  public void clean(final Element el) {
    clearWidth(el);
  }

  private void setWidth(final Element el, final int width) {
    GWT.log("Setting width to: " + width);
    ((HTMLElement) el).style.set("width", String.valueOf(width) + "px");
  }

  private void clearWidth(final Element el) {
    ((HTMLElement) el).style.removeProperty("width");
  }
}
