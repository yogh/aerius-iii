package nl.overheid.aerius.wui.application.components.nav;

import java.util.function.Consumer;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gwt.resources.client.DataResource;

import nl.overheid.aerius.wui.place.Place;
import nl.overheid.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.place.TokenizedPlace;

@AutoValue
public abstract class NavigationItem {
  public abstract String getName();

  @Nullable
  public abstract DataResource getIcon();

  public abstract Predicate<Place> isActive();

  public abstract Consumer<NavigationItem> getAction();

  public abstract String getHref();

  public static NavigationItem create(final String name,
      final DataResource icon,
      final Predicate<Place> active,
      final Consumer<NavigationItem> action,
      final String href) {
    return new AutoValue_NavigationItem(name, icon, active, action, href);
  }

  public static NavigationItem createPlaceItem(final String name,
      final DataResource icon,
      final Predicate<Place> active,
      final PlaceController placeController,
      final TokenizedPlace place) {
    return create(name, icon, active, p -> placeController.goTo(place), place.getToken());
  }
}
