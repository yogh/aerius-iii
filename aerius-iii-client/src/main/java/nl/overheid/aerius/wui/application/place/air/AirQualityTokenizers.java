package nl.overheid.aerius.wui.application.place.air;

import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.AirQualityPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointListPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.MeasurePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.PreferencePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.ResultPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceImportPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceListPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourcePlace;
import nl.overheid.aerius.wui.place.PlaceTokenizer;

public class AirQualityTokenizers {
  private static final String SOURCES = "sources";
  private static final String CALCULATION_POINTS = "calcpoints";
  private static final String MEASURES = "measures";
  private static final String PREFERENCES = "preferences";
  private static final String RESULTS = "results";
  private static final String LAYERS = "layers";

  public static final PlaceTokenizer<SourcePlace> SOURCE = new AirQualityPlace.Tokenizer<>(
      () -> new SourcePlace(),
      SOURCES);

  public static final PlaceTokenizer<SourceImportPlace> SOURCE_IMPORT = new AirQualityPlace.Tokenizer<>(
      () -> new SourceImportPlace(),
      SOURCES, "start");
  public static final PlaceTokenizer<SourceCreatePlace> SOURCE_CREATE = new AirQualityPlace.Tokenizer<>(
      () -> new SourceCreatePlace(),
      SOURCES, "create");
  public static final PlaceTokenizer<SourceListPlace> SOURCE_LIST = new AirQualityPlace.Tokenizer<>(
      () -> new SourceListPlace(),
      SOURCES, "list");

  public static final PlaceTokenizer<CalculationPointPlace> CALCULATION_POINT = new AirQualityPlace.Tokenizer<>(
      () -> new CalculationPointPlace(),
      CALCULATION_POINTS);
  public static final PlaceTokenizer<CalculationPointCreatePlace> RECEPTOR_CREATE = new AirQualityPlace.Tokenizer<>(
      () -> new CalculationPointCreatePlace(),
      CALCULATION_POINTS, "create");
  public static final PlaceTokenizer<CalculationPointListPlace> RECEPTOR_LIST = new AirQualityPlace.Tokenizer<>(
      () -> new CalculationPointListPlace(),
      CALCULATION_POINTS, "list");

  public static final PlaceTokenizer<MeasurePlace> MEASURE = new AirQualityPlace.Tokenizer<>(
      () -> new MeasurePlace(),
      MEASURES);

  public static final PlaceTokenizer<PreferencePlace> PREFERENCE = new AirQualityPlace.Tokenizer<>(
      () -> new PreferencePlace(),
      PREFERENCES);
  public static final PlaceTokenizer<ResultPlace> RESULT = new AirQualityPlace.Tokenizer<>(
      () -> new ResultPlace(),
      RESULTS);
  public static final PlaceTokenizer<ResultPlace> LAYER = new AirQualityPlace.Tokenizer<>(
      () -> new ResultPlace(),
      LAYERS);
}
