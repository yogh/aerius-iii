package nl.overheid.aerius.wui.application.ui.deposition.start;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.wui.application.components.dummy.SimpleDummyComponentFactory;
import nl.overheid.aerius.wui.application.place.deposition.DepositionSourceCreatePlace;
import nl.overheid.aerius.wui.application.ui.deposition.DepositionSubActivity;
import nl.overheid.aerius.wui.application.ui.deposition.DepositionView;
import nl.overheid.aerius.wui.place.PlaceController;

public class DepositionStartActivity implements DepositionSubActivity {
  @Inject PlaceController placeController;

  @Inject
  public DepositionStartActivity(@Assisted final DepositionView view) {
    view.setComponent(DepositionStartViewFactory.get(), this);
    view.setMap(SimpleDummyComponentFactory.get());
  }

  public void goToCreate() {
    placeController.goTo(new DepositionSourceCreatePlace());
  }
}
