package nl.overheid.aerius.wui.application.place;

import java.util.EnumMap;
import java.util.function.Supplier;

import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourcePlace;
import nl.overheid.aerius.wui.application.place.deposition.DepositionSourcePlace;
import nl.overheid.aerius.wui.application.place.theme.HealthPlace;
import nl.overheid.aerius.wui.application.place.theme.OdorPlace;
import nl.overheid.aerius.wui.application.util.ThemeOption;

public class ThemePlaceProvider {
  EnumMap<ThemeOption, Supplier<? extends MainThemePlace>> themes;

  public ThemePlaceProvider() {
    themes = new EnumMap<>(ThemeOption.class);
    themes.put(ThemeOption.AIR_QUALITY, SourcePlace::new);
    themes.put(ThemeOption.DEPOSITION, DepositionSourcePlace::new);
    themes.put(ThemeOption.HEALTH, HealthPlace::new);
    themes.put(ThemeOption.ODOR, OdorPlace::new);
    themes.put(ThemeOption.SAFETY, OdorPlace::new);
    themes.put(ThemeOption.SOIL, OdorPlace::new);
    themes.put(ThemeOption.SOUND, OdorPlace::new);
    themes.put(ThemeOption.WATER, OdorPlace::new);
  }

  public MainThemePlace getPlace(final ThemeOption option) {
    return themes.get(option).get();
  }
}
