package nl.overheid.aerius.wui.application.ui.deposition;

import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.activity.DelegableActivity;
import nl.overheid.aerius.wui.application.components.nav.NavigationConfiguration;
import nl.overheid.aerius.wui.application.components.nav.NavigationItem;
import nl.overheid.aerius.wui.application.i18n.M;
import nl.overheid.aerius.wui.application.place.OverviewPlace;
import nl.overheid.aerius.wui.application.place.deposition.DepositionPlace;
import nl.overheid.aerius.wui.application.place.deposition.DepositionSourceCreatePlace;
import nl.overheid.aerius.wui.application.place.deposition.DepositionSourceImportPlace;
import nl.overheid.aerius.wui.application.place.deposition.DepositionSourceListPlace;
import nl.overheid.aerius.wui.application.place.deposition.DepositionSourcePlace;
import nl.overheid.aerius.wui.application.resources.R;
import nl.overheid.aerius.wui.command.PlaceChangeCommand;
import nl.overheid.aerius.wui.place.Place;
import nl.overheid.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.vue.activity.AbstractVueActivity;

public class DepositionActivity extends AbstractVueActivity<DepositionPresenter, DepositionView, DepositionViewFactory>
    implements DepositionPresenter, DelegableActivity {

  @Inject PlaceController placeController;

  private final DepositionActivityManager delegator;

  @Inject
  public DepositionActivity(final DepositionActivityManager delegator) {
    super(DepositionViewFactory.get());
    this.delegator = delegator;
  }

  @Override
  public DepositionPresenter getPresenter() {
    return this;
  }

  @Override
  public void goToOverview() {
    placeController.goTo(new OverviewPlace());
  }

  @Override
  public boolean delegate(final EventBus eventBus, final PlaceChangeCommand c) {
    return delegator.delegate(eventBus, c.getValue(), c::setRedirect);
  }

  @Override
  public void setView(final DepositionView view) {
    delegator.setView(view);

    final NavigationConfiguration nav = new NavigationConfiguration();

    nav.add(NavigationItem.createPlaceItem(M.messages().themeDepositionSources(), R.images().iconSources(),
        p -> p instanceof DepositionPlace,
        placeController, new DepositionSourcePlace()));
    nav.add(NavigationItem.createPlaceItem(M.messages().themeDepositionCalculationPoints(), R.images().iconReceptors(),
        p -> p instanceof DepositionSourceImportPlace,
        placeController, new DepositionSourceImportPlace()));
    nav.add(NavigationItem.createPlaceItem(M.messages().themeDepositionMeasures(), R.images().iconMeasures(),
        p -> p instanceof DepositionSourceCreatePlace,
        placeController, new DepositionSourceCreatePlace()));
    nav.add(NavigationItem.createPlaceItem(M.messages().themeDepositionPreferences(), R.images().iconPreferences(),
        p -> p instanceof DepositionSourceListPlace,
        placeController, new DepositionSourceListPlace()));
    nav.add(NavigationItem.createPlaceItem(M.messages().themeDepositionResults(), R.images().iconResults(),
        p -> p instanceof DepositionSourceListPlace,
        placeController, new DepositionSourceListPlace()));
    nav.add(NavigationItem.createPlaceItem(M.messages().themeDepositionLayers(), R.images().iconLayers(),
        p -> p instanceof DepositionSourceListPlace,
        placeController, new DepositionSourceListPlace()));
    view.setNavigation(nav);
  }

  @Override
  public boolean isDelegable(final Place place) {
    return place instanceof DepositionPlace;
  }
}
