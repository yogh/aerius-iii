package nl.overheid.aerius.wui.application.place.theme;

import nl.overheid.aerius.wui.application.place.MainThemePlace;

public class HealthPlace extends MainThemePlace {
  public HealthPlace() {
    super(ThemeTokenizers.HEALTH);
  }
}
