package nl.overheid.aerius.wui.application.ui.air.receptors.list;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.wui.application.components.dummy.SimpleSecondaryDummyComponentFactory;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointCreatePlace;
import nl.overheid.aerius.wui.application.ui.air.AirQualitySubActivity;
import nl.overheid.aerius.wui.application.ui.air.AirQualityView;
import nl.overheid.aerius.wui.place.PlaceController;

public class ReceptorListActivity implements AirQualitySubActivity {
  @Inject PlaceController placeController;

  @Inject
  public ReceptorListActivity(@Assisted final AirQualityView view) {
    view.setComponent(ReceptorListViewFactory.get(), this);

    view.setMap(SimpleSecondaryDummyComponentFactory.get());
  }

  public void goToImport() {
    placeController.goTo(new CalculationPointCreatePlace());
  }
}
