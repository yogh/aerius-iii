package nl.overheid.aerius.wui.application.ui.air;

import com.google.inject.Inject;

import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointListPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceImportPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceListPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourcePlace;
import nl.overheid.aerius.wui.application.ui.deposition.AbstractSubActivityManager;
import nl.overheid.aerius.wui.dev.GWTProd;
import nl.overheid.aerius.wui.place.Place;

public class AirQualityActivityManager extends AbstractSubActivityManager<AirQualityView, AirQualitySubActivity> {
  private final AirQualityActivityFactory activityFactory;

  @Inject
  public AirQualityActivityManager(final AirQualityActivityFactory activityFactory) {
    this.activityFactory = activityFactory;
  }

  @Override
  public AirQualitySubActivity getActivity(final Place place, final AirQualityView view) {
    if (place instanceof SourceImportPlace) {
      return activityFactory.createAirQualityStartActivity(view, ((SourceImportPlace) place));
    } else if (place instanceof SourceCreatePlace) {
      return activityFactory.createSourceCreateActivity(view, (SourceCreatePlace) place);
    } else if (place instanceof SourceListPlace) {
      return activityFactory.createAirQualityListActivity(view, (SourceListPlace) place);
    } else if (place instanceof CalculationPointCreatePlace) {
      return activityFactory.createReceptorCreateActivity(view, (CalculationPointCreatePlace) place);
    } else if (place instanceof CalculationPointListPlace) {
      return activityFactory.createReceptorListActivity(view, (CalculationPointListPlace) place);
    } else {
      GWTProd.warn("AirQualityActivityManager", "Could not create sub-activity inside AirQualityActivity: no activity for " + place);
      return activityFactory.createEmptyActivity(view);
    }
  }

  @Override
  protected Place getRedirect(final Place place) {
    Place redirect = null;
    if (place.getClass().equals(SourcePlace.class)) {
      redirect = new SourceImportPlace();
    } else if (place.getClass().equals(CalculationPointPlace.class)) {
      redirect = new CalculationPointCreatePlace();
    }

    return redirect;
  }
}
