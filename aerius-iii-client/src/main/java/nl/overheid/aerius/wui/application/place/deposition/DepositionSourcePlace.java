package nl.overheid.aerius.wui.application.place.deposition;

import nl.overheid.aerius.wui.place.PlaceTokenizer;

public class DepositionSourcePlace extends DepositionPlace {
  public DepositionSourcePlace() {
    this(DepositionTokenizers.SOURCE);
  }

  public <P extends DepositionSourcePlace> DepositionSourcePlace(final PlaceTokenizer<P> tokenizer) {
    super(tokenizer);
  }
}
