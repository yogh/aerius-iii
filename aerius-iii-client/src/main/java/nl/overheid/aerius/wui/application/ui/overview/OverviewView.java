package nl.overheid.aerius.wui.application.ui.overview;

import java.util.List;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.gwt.core.client.GWT;

import jsinterop.annotations.JsProperty;
import nl.overheid.aerius.wui.application.components.importer.ApplicationImportComponent;
import nl.overheid.aerius.wui.application.components.nav.HeaderComponent;
import nl.overheid.aerius.wui.application.util.Theme;
import nl.overheid.aerius.wui.application.util.UglyBoilerPlate;
import nl.overheid.aerius.wui.vue.BasicVueView;
import nl.overheid.aerius.wui.vue.MaskDirective;

@Component(
    components = { HeaderComponent.class, ApplicationImportComponent.class },
    directives = MaskDirective.class)
public class OverviewView extends BasicVueView implements HasCreated {
  @Data @JsProperty List<Theme> themes = UglyBoilerPlate.getApplicationThemes();

  @Prop OverviewActivity presenter;

  @Override
  public void created() {
    GWT.log("Presenter set to: " + presenter.toString());
  }
}
