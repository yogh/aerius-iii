package nl.overheid.aerius.wui.application.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.wui.application.util.ThemeOption;

public interface ThemeMessages {
  String themeName(@Select ThemeOption theme);

  String themeAirQualitySources();

  String themeAirQualityCalculationPoints();

  String themeAirQualityMeasures();

  String themeAirQualityPreferences();

  String themeAirQualityResults();

  String themeAirQualityLayers();

  String themeDepositionSources();

  String themeDepositionCalculationPoints();

  String themeDepositionMeasures();

  String themeDepositionPreferences();

  String themeDepositionResults();

  String themeDepositionLayers();
}
