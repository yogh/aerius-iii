package nl.overheid.aerius.wui.application.ui.air;

import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.CalculationPointListPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceCreatePlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceImportPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.SourceListPlace;
import nl.overheid.aerius.wui.application.ui.air.dev.InDevelopmentActivity;
import nl.overheid.aerius.wui.application.ui.air.receptors.create.ReceptorCreateActivity;
import nl.overheid.aerius.wui.application.ui.air.receptors.list.ReceptorListActivity;
import nl.overheid.aerius.wui.application.ui.air.sources.create.SourceCreateActivity;
import nl.overheid.aerius.wui.application.ui.air.sources.list.SourceListActivity;
import nl.overheid.aerius.wui.application.ui.air.sources.start.SourceStartActivity;

public interface AirQualityActivityFactory {
  SourceStartActivity createAirQualityStartActivity(AirQualityView view, SourceImportPlace place);

  SourceCreateActivity createSourceCreateActivity(AirQualityView view, SourceCreatePlace place);

  SourceListActivity createAirQualityListActivity(AirQualityView view, SourceListPlace place);

  ReceptorCreateActivity createReceptorCreateActivity(AirQualityView view, CalculationPointCreatePlace place);

  ReceptorListActivity createReceptorListActivity(AirQualityView view, CalculationPointListPlace place);

  InDevelopmentActivity createEmptyActivity(AirQualityView view);
}
