package nl.overheid.aerius.wui.application.place;

import java.util.function.Supplier;

import nl.overheid.aerius.wui.application.util.ThemeOption;
import nl.overheid.aerius.wui.place.ApplicationPlace;
import nl.overheid.aerius.wui.place.PlaceTokenizer;

public abstract class MainThemePlace extends ApplicationPlace {
  public MainThemePlace(final PlaceTokenizer<? extends ApplicationPlace> tokenizer) {
    super(tokenizer);
  }

  public static class Tokenizer<T extends MainThemePlace> extends ApplicationPlace.Tokenizer<T> {
    public Tokenizer(final Supplier<T> supplier, final ThemeOption theme, final String ... postfix) {
      super(supplier, prepend(theme.getKey(), postfix));
    }
  }

  public <P extends MainThemePlace> P copyTo(final P copy) {
    return copy;
  }
}
