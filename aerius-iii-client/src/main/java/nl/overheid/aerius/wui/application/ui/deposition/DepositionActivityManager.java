package nl.overheid.aerius.wui.application.ui.deposition;

import com.google.inject.Inject;

import nl.overheid.aerius.wui.application.place.deposition.DepositionSourceCreatePlace;
import nl.overheid.aerius.wui.application.place.deposition.DepositionSourceImportPlace;
import nl.overheid.aerius.wui.application.place.deposition.DepositionSourceListPlace;
import nl.overheid.aerius.wui.application.place.deposition.DepositionSourcePlace;
import nl.overheid.aerius.wui.place.Place;

public class DepositionActivityManager extends AbstractSubActivityManager<DepositionView, DepositionSubActivity> {
  private final DepositionActivityFactory activityFactory;

  @Inject
  public DepositionActivityManager(final DepositionActivityFactory activityFactory) {
    this.activityFactory = activityFactory;
  }

  @Override
  protected DepositionSubActivity getActivity(final Place place, final DepositionView view) {
    if (place instanceof DepositionSourceImportPlace) {
      return activityFactory.createDepositionStartActivity(view, ((DepositionSourceImportPlace) place));
    } else if (place instanceof DepositionSourceCreatePlace) {
      return activityFactory.createDepositionCreateActivity(view, (DepositionSourceCreatePlace) place);
    } else if (place instanceof DepositionSourceListPlace) {
      return activityFactory.createDepositionListActivity(view, (DepositionSourceListPlace) place);
    } else {
      throw new RuntimeException("Could not create sub-activity for DepositionActivity");
    }
  }

  @Override
  protected Place getRedirect(final Place place) {
    if (place.getClass().equals(DepositionSourcePlace.class)) {
      return new DepositionSourceImportPlace();
    }

    return null;
  }
}
