package nl.overheid.aerius.wui.application.ui.air.dev;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

@Component
public class InDevelopmentView implements IsVueComponent {
  @Prop InDevelopmentActivity presenter;
}
