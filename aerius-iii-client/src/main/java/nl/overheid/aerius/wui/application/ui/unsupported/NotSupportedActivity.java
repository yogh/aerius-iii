package nl.overheid.aerius.wui.application.ui.unsupported;

import com.google.inject.Inject;

import nl.overheid.aerius.wui.vue.activity.AbstractVueActivity;

public class NotSupportedActivity extends AbstractVueActivity<NotSupportedActivity, NotSupportedView, NotSupportedViewFactory> {
  @Inject
  public NotSupportedActivity(final NotSupportedViewFactory factory) {
    super(factory);
  }

  @Override
  public NotSupportedActivity getPresenter() {
    return this;
  }
}
