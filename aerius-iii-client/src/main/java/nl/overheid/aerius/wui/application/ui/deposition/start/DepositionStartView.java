package nl.overheid.aerius.wui.application.ui.deposition.start;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;

import nl.overheid.aerius.wui.application.components.dummy.SimpleDummyComponent;
import nl.overheid.aerius.wui.application.components.dummy.SimpleSecondaryDummyComponent;
import nl.overheid.aerius.wui.application.components.views.DefaultTitleView;
import nl.overheid.aerius.wui.vue.activity.VueView;

@Component(components = { DefaultTitleView.class,
    SimpleDummyComponent.class,
    SimpleSecondaryDummyComponent.class
})
public class DepositionStartView implements VueView<DepositionStartActivity> {
  @Prop DepositionStartActivity presenter;
}
