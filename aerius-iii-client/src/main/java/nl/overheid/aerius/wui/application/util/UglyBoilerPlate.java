package nl.overheid.aerius.wui.application.util;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.wui.application.resources.R;

public class UglyBoilerPlate {
  public static List<Theme> getApplicationThemes() {
    final List<Theme> themes = new ArrayList<>();

    themes.add(Theme.create(ThemeOption.SOIL, R.images().themeGround(), false));
    themes.add(Theme.create(ThemeOption.SAFETY, R.images().themExternalSafety(), false));
    themes.add(Theme.create(ThemeOption.AIR_QUALITY, R.images().themeAirQuality(), true));
    themes.add(Theme.create(ThemeOption.SOUND, R.images().themeSound(), false));
    themes.add(Theme.create(ThemeOption.ODOR, R.images().themeOdor(), false));
    themes.add(Theme.create(ThemeOption.HEALTH, R.images().themeHealth(), false));
    themes.add(Theme.create(ThemeOption.DEPOSITION, R.images().themeNitrogenDeposition(), true));
    themes.add(Theme.create(ThemeOption.WATER, R.images().themeWater(), false));

    return themes;
  }
}
