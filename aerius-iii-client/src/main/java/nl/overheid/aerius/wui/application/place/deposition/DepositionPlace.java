package nl.overheid.aerius.wui.application.place.deposition;

import java.util.function.Supplier;

import nl.overheid.aerius.wui.application.place.MainThemePlace;
import nl.overheid.aerius.wui.application.util.ThemeOption;
import nl.overheid.aerius.wui.place.PlaceTokenizer;

public class DepositionPlace extends MainThemePlace {
  public static class Tokenizer<T extends DepositionPlace> extends MainThemePlace.Tokenizer<T> {
    public Tokenizer(final Supplier<T> provider, final String... postfix) {
      super(provider, ThemeOption.DEPOSITION, postfix);
    }
  }

  public <P extends DepositionPlace> DepositionPlace(final PlaceTokenizer<P> tokenizer) {
    super(tokenizer);
  }
}
