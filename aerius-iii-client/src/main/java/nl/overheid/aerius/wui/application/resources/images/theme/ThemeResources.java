package nl.overheid.aerius.wui.application.resources.images.theme;

import com.google.gwt.resources.client.ClientBundle.Source;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.DataResource.MimeType;

public interface ThemeResources {
  @Source("theme-external-safety.svg")
  @MimeType("image/svg+xml")
  DataResource themExternalSafety();

  @Source("theme-ground.svg")
  @MimeType("image/svg+xml")
  DataResource themeGround();

  @Source("theme-health.svg")
  @MimeType("image/svg+xml")
  DataResource themeHealth();

  @Source("theme-nitrogen-deposition.svg")
  @MimeType("image/svg+xml")
  DataResource themeNitrogenDeposition();

  @Source("theme-odor.svg")
  @MimeType("image/svg+xml")
  DataResource themeOdor();

  @Source("theme-particulate-matter.svg")
  @MimeType("image/svg+xml")
  DataResource themeAirQuality();

  @Source("theme-sound.svg")
  @MimeType("image/svg+xml")
  DataResource themeSound();

  @Source("theme-water.svg")
  @MimeType("image/svg+xml")
  DataResource themeWater();
}
