package nl.overheid.aerius.wui.application.components.nav;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import nl.overheid.aerius.wui.place.Place;
import nl.overheid.aerius.wui.vue.MaskDirective;

@Component(name = "aer-nav-item",
    directives = { MaskDirective.class })
public class NavigationItemComponent implements IsVueComponent {
  @Prop NavigationItem item;

  @Prop Place place;

  @Computed("isActive")
  public boolean isActive() {
    return item.isActive().test(place);
  }
}
