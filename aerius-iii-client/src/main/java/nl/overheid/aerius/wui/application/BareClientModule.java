package nl.overheid.aerius.wui.application;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;
import com.google.gwt.place.shared.PlaceHistoryHandler.Historian;
import com.google.inject.TypeLiteral;

import nl.overheid.aerius.wui.activity.ActivityManager;
import nl.overheid.aerius.wui.activity.ActivityMapper;
import nl.overheid.aerius.wui.application.activity.ApplicationActivityFactory;
import nl.overheid.aerius.wui.application.activity.ApplicationActivityMapper;
import nl.overheid.aerius.wui.application.activity.ApplicationPlaceHistoryMapper;
import nl.overheid.aerius.wui.application.daemon.ApplicationDaemonBootstrapper;
import nl.overheid.aerius.wui.application.dev.ApplicationDevelopmentObserver;
import nl.overheid.aerius.wui.application.place.OverviewPlace;
import nl.overheid.aerius.wui.application.ui.air.AirQualityActivityFactory;
import nl.overheid.aerius.wui.application.ui.deposition.DepositionActivityFactory;
import nl.overheid.aerius.wui.daemon.DaemonBootstrapper;
import nl.overheid.aerius.wui.dev.DevelopmentObserver;
import nl.overheid.aerius.wui.history.HTML5Historian;
import nl.overheid.aerius.wui.history.PlaceHistoryMapper;
import nl.overheid.aerius.wui.place.ApplicationPlace;
import nl.overheid.aerius.wui.place.DefaultPlace;
import nl.overheid.aerius.wui.vue.AcceptsOneComponent;
import nl.overheid.aerius.wui.vue.activity.VueActivityManager;

class BareClientModule extends AbstractGinModule {
  @Override
  protected void configure() {
    bind(ApplicationPlace.class).annotatedWith(DefaultPlace.class).to(OverviewPlace.class);
    bind(Historian.class).to(HTML5Historian.class);

    bind(new TypeLiteral<ActivityManager<AcceptsOneComponent>>() {}).to(VueActivityManager.class);
    bind(new TypeLiteral<ActivityMapper<AcceptsOneComponent>>() {}).to(ApplicationActivityMapper.class);
    bind(DaemonBootstrapper.class).to(ApplicationDaemonBootstrapper.class);
    bind(PlaceHistoryMapper.class).to(ApplicationPlaceHistoryMapper.class);
    bind(DevelopmentObserver.class).to(ApplicationDevelopmentObserver.class);

    // Bind factories
    install(new GinFactoryModuleBuilder().build(ApplicationActivityFactory.class));
    install(new GinFactoryModuleBuilder().build(DepositionActivityFactory.class));
    install(new GinFactoryModuleBuilder().build(AirQualityActivityFactory.class));
  }
}
