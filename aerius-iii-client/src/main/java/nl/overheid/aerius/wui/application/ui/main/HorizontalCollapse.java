package nl.overheid.aerius.wui.application.ui.main;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import elemental2.dom.Element;
import elemental2.dom.HTMLElement;
import jsinterop.annotations.JsMethod;

@Component
public class HorizontalCollapse implements IsVueComponent {
  @JsMethod
  public void enter(final Element el) {
    setWidth(el, ((HTMLElement) el).scrollWidth);
  }

  @JsMethod
  public void clean(final Element el) {
    clearWidth(el);
  }

  @JsMethod
  public void beforeLeave(final Element el) {
    setWidth(el, el.clientWidth);
  }

  private void setWidth(final Element el, final int width) {
    ((HTMLElement) el).style.set("width", String.valueOf(width) + "px");
  }

  private void clearWidth(final Element el) {
    ((HTMLElement) el).style.removeProperty("width");
  }
}
