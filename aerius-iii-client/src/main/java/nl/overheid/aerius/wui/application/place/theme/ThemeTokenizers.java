package nl.overheid.aerius.wui.application.place.theme;

import nl.overheid.aerius.wui.application.place.MainThemePlace;
import nl.overheid.aerius.wui.application.util.ThemeOption;
import nl.overheid.aerius.wui.place.PlaceTokenizer;

public class ThemeTokenizers {
  public static final PlaceTokenizer<HealthPlace> HEALTH = new MainThemePlace.Tokenizer<>(
      () -> new HealthPlace(), ThemeOption.HEALTH);
  public static final PlaceTokenizer<OdorPlace> ODOR = new MainThemePlace.Tokenizer<>(
      () -> new OdorPlace(), ThemeOption.ODOR);
}
