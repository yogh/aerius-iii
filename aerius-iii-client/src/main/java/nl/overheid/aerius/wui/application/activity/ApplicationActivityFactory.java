package nl.overheid.aerius.wui.application.activity;

import nl.overheid.aerius.wui.application.place.OverviewPlace;
import nl.overheid.aerius.wui.application.place.air.AirQualityPlaces.AirQualityPlace;
import nl.overheid.aerius.wui.application.place.deposition.DepositionPlace;
import nl.overheid.aerius.wui.application.ui.air.AirQualityActivity;
import nl.overheid.aerius.wui.application.ui.deposition.DepositionActivity;
import nl.overheid.aerius.wui.application.ui.overview.OverviewActivity;
import nl.overheid.aerius.wui.application.ui.unsupported.NotSupportedActivity;

public interface ApplicationActivityFactory {
  OverviewActivity createOverviewActivity(OverviewPlace place);

  AirQualityActivity createAirQualityActivity(AirQualityPlace place);

  NotSupportedActivity createNotSupportedActivity();

  DepositionActivity createDepositionActivity(DepositionPlace place);
}
