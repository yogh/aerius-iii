package nl.overheid.aerius.wui.application.components.logo;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;

import jsinterop.annotations.JsMethod;
import nl.overheid.aerius.wui.application.place.OverviewPlace;
import nl.overheid.aerius.wui.application.resources.ImageResources;
import nl.overheid.aerius.wui.application.resources.R;
import nl.overheid.aerius.wui.place.PlaceController;

@Component(name = "aer-logo")
public class AeriusLogoComponent implements IsVueComponent {
  @Inject PlaceController placeController;

  @Computed
  public ImageResources img() {
    return R.images();
  }

  @JsMethod
  public void onLogoClick() {
    GWT.log(placeController + " < PlaceController");
    placeController.goTo(new OverviewPlace());
  }
}
