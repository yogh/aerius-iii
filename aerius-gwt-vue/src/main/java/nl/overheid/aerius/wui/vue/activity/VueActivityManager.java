package nl.overheid.aerius.wui.vue.activity;

import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.activity.AbstractActivityManager;
import nl.overheid.aerius.wui.activity.ActivityMapper;
import nl.overheid.aerius.wui.place.PlaceController;
import nl.overheid.aerius.wui.vue.AcceptsOneComponent;

public class VueActivityManager extends AbstractActivityManager<AcceptsOneComponent> {
  @Inject
  public VueActivityManager(final EventBus globalEventBus, final PlaceController placeController, final ActivityMapper<AcceptsOneComponent> mapper) {
    super(globalEventBus, placeController, mapper);
  }
}
