package nl.overheid.aerius.wui.vue;

import com.axellience.vuegwt.core.client.vue.VueComponentFactory;

public interface AcceptsOneComponent {
  <P> void setComponent(VueComponentFactory<?> factory, P presenter);
}
