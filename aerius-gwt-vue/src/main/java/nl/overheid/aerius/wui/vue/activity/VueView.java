package nl.overheid.aerius.wui.vue.activity;

import com.axellience.vuegwt.core.client.component.IsVueComponent;

public interface VueView<P> extends IsVueComponent {}
