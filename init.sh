command -v mvn >/dev/null 2>&1 || { echo >&2 "I require 'mvn' but it's not installed.  Aborting."; exit 1; }
command -v git >/dev/null 2>&1 || { echo >&2 "I require 'git' but it's not installed.  Aborting."; exit 1; }

git submodule init
git submodule update --remote

mvn install -Pdependencies
