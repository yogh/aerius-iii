command -v tmux >/dev/null 2>&1 || { echo >&2 "I require tmux but it's not installed.  Aborting."; exit 1; }
command -v entr >/dev/null 2>&1 || { echo >&2 "I require entr but it's not installed.  Aborting."; exit 1; }

cd scripts

tmux kill-session -t aerius-iii-serve
tmux new -d ./codeserver.sh -s aerius-iii-serve
tmux split-window -v ./webserver.sh
tmux split-window -h ./livereload.sh
tmux -2 attach-session -d
