package nl.overheid.aerius.wui.activators;

public interface IsActivatorInfo {
  String getName();
}
