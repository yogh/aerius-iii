package nl.overheid.aerius.wui.daemon;

import nl.overheid.aerius.wui.widget.HasEventBus;

public interface DaemonBootstrapper extends HasEventBus {}
