package nl.overheid.aerius.wui.dev;

public class GWTProd {
  /**
   * Logs a message to the console. Calls are _not_ optimized out in Production Mode.
   */
  public static native void log(String message) /*-{
    console.log(message );
  }-*/;

  /**
   * Logs a message to the warn console. Calls are _not_ optimized out in Production Mode.
   */
  public static native void warn(String message) /*-{
    console.warn(message );
  }-*/;

  public static void warn(final String marker, final String string) {
    warn("[" + marker + "] " + string);
  }

  public static void log(final String marker, final String string) {
    log("[" + marker + "] " + string);
  }
}
