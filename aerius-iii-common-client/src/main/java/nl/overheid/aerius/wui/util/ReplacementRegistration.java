package nl.overheid.aerius.wui.util;

public interface ReplacementRegistration {
  void unregister();
}